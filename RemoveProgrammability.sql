DECLARE @truncateQuery nvarchar(max)
DECLARE @truncateCursor CURSOR

--drop FK constraints for adm tables
SET @truncateCursor = CURSOR FOR
SELECT 'ALTER TABLE dbo.['+TABLE_NAME+'] DROP CONSTRAINT ['+CONSTRAINT_NAME+']' FROM
(
	SELECT DISTINCT TABLE_NAME, CONSTRAINT_NAME
	FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE
	WHERE CONSTRAINT_SCHEMA = 'dbo'
		AND CONSTRAINT_NAME NOT LIKE 'PK_%'
		AND CONSTRAINT_NAME NOT LIKE 'UQ_%'
		AND (TABLE_NAME LIKE '%adm%' OR CONSTRAINT_NAME LIKE 'FK_adm%' OR CONSTRAINT_NAME LIKE '%_adm%')
) cn

OPEN @truncateCursor
FETCH NEXT
FROM @truncateCursor INTO @truncateQuery
WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC sp_executesql @stmt=@truncateQuery
	FETCH NEXT
	FROM @truncateCursor INTO @truncateQuery
END
CLOSE @truncateCursor
DEALLOCATE @truncateCursor

-- drop default constraints
SET @truncateCursor = CURSOR FOR
SELECT 'ALTER TABLE dbo.['+tab+'] DROP CONSTRAINT ['+def+']'
FROM (
	SELECT DISTINCT dc.name as def, tb.name as tab --dc.name
	FROM sys.default_constraints dc
		INNER JOIN sys.schemas sh ON sh.schema_id = dc.schema_id
		INNER JOIN sys.tables tb ON tb.object_id = dc.parent_object_id
	WHERE sh.name = 'dbo'
) x

OPEN @truncateCursor
FETCH NEXT
FROM @truncateCursor INTO @truncateQuery
WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC sp_executesql @stmt=@truncateQuery
	FETCH NEXT
	FROM @truncateCursor INTO @truncateQuery
END
CLOSE @truncateCursor
DEALLOCATE @truncateCursor


--drop stored procedures and functions
SET @truncateCursor = CURSOR FOR
SELECT 'DROP PROCEDURE dbo.['+ROUTINE_NAME+']' FROM INFORMATION_SCHEMA.ROUTINES
WHERE ROUTINE_SCHEMA='dbo' AND ROUTINE_TYPE = 'PROCEDURE'

OPEN @truncateCursor
FETCH NEXT
FROM @truncateCursor INTO @truncateQuery
WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC sp_executesql @stmt=@truncateQuery
	FETCH NEXT
	FROM @truncateCursor INTO @truncateQuery
END
CLOSE @truncateCursor
DEALLOCATE @truncateCursor


--drop triggers
SET @truncateCursor = CURSOR FOR
SELECT 'DROP TRIGGER [dbo].['+name+']'
FROM(
	SELECT DISTINCT tr.name
	FROM sys.triggers tr
		INNER JOIN sys.tables tb ON tb.object_id = tr.parent_id
		INNER JOIN sys.schemas sh ON sh.schema_id = tb.schema_id
	WHERE sh.name = 'dbo'
) x

OPEN @truncateCursor
FETCH NEXT
FROM @truncateCursor INTO @truncateQuery
WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC sp_executesql @stmt=@truncateQuery
	FETCH NEXT
	FROM @truncateCursor INTO @truncateQuery
END
CLOSE @truncateCursor
DEALLOCATE @truncateCursor


--DROP adm tables
SET @truncateCursor = CURSOR FOR
SELECT 'DROP TABLE dbo.['+TABLE_NAME+']'
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA='dbo'
	AND TABLE_NAME LIKE 'adm%'

OPEN @truncateCursor
FETCH NEXT
FROM @truncateCursor INTO @truncateQuery
WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC sp_executesql @stmt=@truncateQuery
	FETCH NEXT
	FROM @truncateCursor INTO @truncateQuery
END
CLOSE @truncateCursor
DEALLOCATE @truncateCursor
