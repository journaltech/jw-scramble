/* Data scramble script
	Last updated: 2015-10-19

	When script is complete, run the summary rebuild
	  tools in the maintenance console

	Things that are scrambled:
	  ~ tblNameActual:
		# First Name
		# Middle Name
		# Last Name
		# All name numbers
		# All contact info

	  ~ tblCaseActual
	    # Case Title

*/

DELETE FROM tblNameSummary
DELETE FROM tblCaseSummary
DELETE FROM ltblPhoneActual
DELETE FROM ltblNameActual
GO

-- Create necessary views and functions
IF (OBJECT_ID('dbo.devRandomNumber') IS NOT NULL) DROP VIEW dbo.devRandomNumber
GO

CREATE VIEW dbo.devRandomNumber AS
	SELECT RAND() AS RandomNumber
GO

IF (OBJECT_ID('dbo.devNewID') IS NOT NULL) DROP VIEW dbo.devNewID
GO

CREATE VIEW dbo.devNewID AS
	SELECT NewID() AS "NewID"
GO

IF (OBJECT_ID('dbo.fnGetRandomNumber') IS NOT NULL) DROP FUNCTION dbo.fnGetRandomNumber
GO

CREATE FUNCTION dbo.fnGetRandomNumber
(
	@NumOfDigits AS TINYINT
)
RETURNS BIGINT AS BEGIN
	IF (@NumOfDigits > 14) BEGIN
		-- return an error
		RETURN CAST('Function cannot exceed 14 digits' AS INT);
	END

	DECLARE @ReturnValue AS BIGINT
	DECLARE @RandNum AS FLOAT
	SELECT @RandNum = RandomNumber FROM dbo.devRandomNumber
	DECLARE @BigRandNum AS BIGINT = @RandNum * 100000000000000
	DECLARE @RandString AS VARCHAR(55) = CAST(@BigRandNum AS VARCHAR(14))

	WHILE (LEN(@RandString) < @NumOfDigits) BEGIN
		SET @RandString = @RandString + CAST((SELECT dbo.fnGetRandomNumber(14)) AS VARCHAR(14))
	END

	SET @RandString = RIGHT(RTRIM(@RandString), @NumOfDigits)
	SET @ReturnValue = CAST(@RandString AS BIGINT);

	RETURN @ReturnValue;
END
GO

IF (OBJECT_ID('vw_NewGuid') IS NOT NULL) DROP VIEW vw_NewGuid
GO

CREATE VIEW vw_NewGuid AS 
(
	SELECT NEWID() AS new_guid
)
GO

IF (OBJECT_ID('dbo.ipsumLoremGen') IS NOT NULL) DROP FUNCTION dbo.ipsumLoremGen
GO

CREATE FUNCTION dbo.ipsumLoremGen
(
	@NumOfWords as INT
)
RETURNS VARCHAR(MAX) AS BEGIN
	IF (@NumOfWords > 20) BEGIN
		SET @NumOfWords = 20
	END
	
	DECLARE @ipsumLoremTable TABLE
	(
		id int IDENTITY(1,1) PRIMARY KEY,
		word varchar(50)
	)
	INSERT INTO @ipsumLoremTable (word)
	VALUES
	('Lorem '),('ipsum '),('dolor '),('sit '),('amet, '),('consectetur '),('adipiscing '),('elit, '),('sed '),('do '),('eiusmod '),('tempor '),
	('incididunt '),('ut '),('labore '),('et '),('dolore '),('magna '),('aliqua. '),('Ut '),('enim '),('ad '),('minim '),('veniam, '),('quis '),
	('nostrud '),('exercitation '),('ullamco '),('laboris '),('nisi '),('ut '),('aliquip '),('ex '),('ea '),('commodo '),('consequat. '),('Duis '),
	('aute '),('irure '),('dolor '),('in '),('reprehenderit '),('in '),('voluptate '),('velit '),('esse '),('cillum '),('dolore '),('eu '),('fugiat '),
	('nulla '),('pariatur. '),('Excepteur '),('sint '),('occaecat '),('cupidatat '),('non '),('proident, '),('sunt '),('in '),('culpa '),('qui '),('officia '),
	('deserunt '),('mollit '),('anim '),('id '),('est '),('laborum. '),('Curabitur '),('pretium '),('tincidunt '),('lacus. '),('Nulla '),('gravida '),('orci '),
	('a '),('odio. '),('Nullam '),('varius, '),('turpis '),('et '),('commodo '),('pharetra, '),('est '),('eros '),('bibendum '),('elit, '),('nec '),
	('luctus '),('magna '),('felis '),('sollicitudin '),('mauris. '),('Integer '),('in '),('mauris '),('eu '),('nibh '),('euismod '),('gravida. '),
	('Duis '),('ac '),('tellus '),('et '),('risus '),('vulputate '),('vehicula. '),('Donec '),('lobortis '),('risus '),('a '),('elit. '),('Etiam '),
	('tempor. '),('Ut '),('ullamcorper, '),('ligula '),('eu '),('tempor '),('congue, '),('eros '),('est '),('euismod '),('turpis, '),('id '),('tincidunt '),
	('sapien '),('risus '),('a '),('quam. '),('Maecenas '),('fermentum '),('consequat '),('mi. '),('Donec '),('fermentum. '),('Pellentesque '),('malesuada '),
	('nulla '),('a '),('mi. '),('Duis '),('sapien '),('sem, '),('aliquet '),('nec, '),('commodo '),('eget, '),('consequat '),('quis, '),('neque. '),
	('Aliquam '),('faucibus, '),('elit '),('ut '),('dictum '),('aliquet, '),('felis '),('nisl '),('adipiscing '),('sapien, '),('sed '),('malesuada '),
	('diam '),('lacus '),('eget '),('erat. '),('Cras '),('mollis '),('scelerisque '),('nunc. '),('Nullam '),('arcu. '),('Aliquam '),('consequat. '),
	('Curabitur '),('augue '),('lorem, '),('dapibus '),('quis, '),('laoreet '),('et, '),('pretium '),('ac, '),('nisi. '),('Aenean '),('magna '),('nisl, '),
	('mollis '),('quis, '),('molestie '),('eu, '),('feugiat '),('in, '),('orci. '),('In '),('hac '),('habitasse '),('platea '),('dictumst. ')

	DECLARE @IpsumCount INT
	SELECT @IpsumCount = COUNT(*) FROM @ipsumLoremTable

	DECLARE @appendCount INT
	SET @appendCount = 0
	DECLARE @ipsumPhrase varchar(MAX)
	SET @ipsumPhrase = ''
	DECLARE @ipsumID INT
	WHILE @appendCount < @NumOfWords
	BEGIN
		SET @appendCount = @appendCount + 1
		DECLARE @NewID1 AS VARCHAR(36) = (SELECT new_guid FROM vw_NewGuid);
		SET @ipsumID = abs(checksum(@NewID1))%@IpsumCount +1
		SET @ipsumPhrase = @ipsumPhrase + (SELECT word FROM @ipsumLoremTable where id = @ipsumID)
		--SET @ipsumPhrase = (SELECT word FROM @ipsumLoremTable where id = @ipsumID)
		--PRINT @appendCount
		--PRINT @ipsumPhrase
	END
	RETURN @ipsumPhrase;
END
GO

IF (OBJECT_ID('dbo.fnGetRandomString') IS NOT NULL) DROP FUNCTION dbo.fnGetRandomString
GO

CREATE FUNCTION dbo.fnGetRandomString
(
	@NumOfChars AS TINYINT
)
RETURNS VARCHAR(64) AS BEGIN
	IF (@NumOfChars > 64) BEGIN
		-- return an error
		RETURN CAST('Function cannot exceed 64 characters' AS INT);
	END

	DECLARE @NewID1 AS VARCHAR(36) = (SELECT NewID FROM dbo.devNewID);
	DECLARE @NewID2 AS VARCHAR(36) = (SELECT NewID FROM dbo.devNewID);
	DECLARE @ReturnValue AS VARCHAR(64)= REPLACE(@NewID1, '-', '') + REPLACE(@NewID2, '-', '');

	IF (@NumOfChars < 32) BEGIN
		DECLARE @StartPos AS TINYINT = (64 / 2) - @NumOfChars;
		SET @ReturnValue = SUBSTRING(@ReturnValue, @StartPos, @NumOfChars);
	END ELSE BEGIN
		SET @ReturnValue = LEFT(@ReturnValue, @NumOfChars);
	END

	RETURN @ReturnValue;
END
GO
SELECT dbo.fnGetRandomString(12);

IF (OBJECT_ID('dbo.fnGetNewNumber') IS NOT NULL) DROP FUNCTION dbo.fnGetNewNumber
GO

CREATE FUNCTION dbo.fnGetNewNumber
(
	@Len AS INT
	, @Type AS UdtLgCode
)
RETURNS VARCHAR(64) AS BEGIN
	DECLARE @ReturnValue VARCHAR(64)

	IF (@Type = 'SSN') BEGIN
		DECLARE @Num1 AS VARCHAR(3) = CAST(dbo.fnGetRandomNumber(3) AS VARCHAR(3));
		DECLARE @Num2 AS VARCHAR(2) = CAST(dbo.fnGetRandomNumber(3) AS VARCHAR(3));
		DECLARE @Num3 AS VARCHAR(4) = CAST(dbo.fnGetRandomNumber(3) AS VARCHAR(3));
		SET @ReturnValue = @Num1 + '-' + @Num2 + '-' + @Num3
	END ELSE BEGIN
		SET @ReturnValue = dbo.fnGetRandomString(@Len);
	END

	RETURN @ReturnValue;
END
GO

DECLARE @nametemp TABLE
(
	id int IDENTITY(1,1) PRIMARY KEY,
	name varchar(50)
)

--characters from romeo, hamlet, macbeth, othello
--characters with at least 10 lines
--source: https://www.opensourceshakespeare.org/views/plays/characters/chardisplay.php
INSERT INTO @nametemp (name)
VALUES
('Balthasar'),('Banquo'),('Benvolio'),('Bernardo'),('Bianca'),('Brabantio'),
('Capulet'),('Cassio'),('Claudius'),
('Desdemona'),('Duke of Venice'),('Duncan'),
('Emilia'),
('Father''s Ghost'),('First Clown'),('First Murderer'),('First Musician'),('First Witch'),('Friar Laurence'),
('Gertrude'),('Gratiano'),('Gregory'),('Guildenstern'),
('Hamlet'),('Horatio'),
('Iago'),
('Juliet'),
('Lady Capulet'),('Lady Macbeth'),('Lady Macduff'),('Laertes'),('Lennox'),('Lodovico'),
('Macbeth'),('Macduff'),('Malcolm'),('Marcellus'),('Mercutio'),('Montague'),('Montano'),
('Ophelia'),('Osric'),('Othello'),
('Paris'),('Peter'),('Polonius'),('Prince Escalus'),
('Reynaldo'),('Roderigo'),('Romeo'),('Rosencrantz'),('Ross'),
('Sampson'),('Second Clown'),('Second Witch'),('Siward'),
('Third Witch'),('Tybalt')

DECLARE @namecount INT
SELECT @namecount = COUNT(*) FROM @nametemp

DECLARE @firstnametemp TABLE
(
	id int IDENTITY(1,1) PRIMARY KEY,
	name varchar(50)
)
--names from the D&D 5e gods section of the player's handbook
INSERT INTO @firstnametemp (name)
VALUES
('Auril'),('Azuth'),('Bane'),('Beshaba'),('Bhaal'),('Chauntea'),('Cyric'),('Deneir'),('Eldath'),('Gond'),('Helm'),('Ilmater'),('Kelemvor'),
('Lathander'),('Leira'),('Lliira'),('Loviatar'),('Malar'),('Mask'),('Mielikki'),('Milil'),('Myrkul'),('Mystra'),('Oghma'),
('Savras'),('Selune'),('Shar'),('Silvanus'),('Sune'),('Talona'),('Talos'),('Tempus'),('Torm'),('Tymora'),('Tyr'),
('Umberlee'),('Waukeen'),('Beory'),('Boccob'),('Celestian'),('Ehlonna'),('Erythnul'),('Fharlanghn'),('Heironeous'),
('Hextor'),('Kord'),('Incabulos'),('Istus'),('Iuz'),('Nerull'),('Obad-Hai'),('Olidammara'),('Pelor'),('Pholtus'),
('Ralishaz'),('Rao'),('St. Cuthbert'),('Tharizdun'),('Trithereon'),('Ulaa'),('Vecna'),('Wee Jas'),('Paladine'),
('Branchala'),('Habbakuk'),('Kiri-Jolith'),('Majere'),('Mishakal'),('Solinari'),('Gilean'),('Chislev'),('Reorx'),
('Shinare'),('Sirrion'),('Zivilyn'),('Lunitari'),('Takhisis'),('Chemosh'),('Hiddukel'),('Morgion'),('Sargonnas'),
('Zeboim'),('Nuitari'),('Arawai'),('Aureon'),('Balinor'),('Boldrei'),('Dol Arrah'),('Dol Dorn'),('Kol Korran'),('Olladra'),
('Onatar'),('The Devourer'),('The Fury'),('The Keeper'),('The Mockery'),('The Shadow'),('The Traveler'),('The Silver Flame'),
('The Blood of Vol'),('Cults of the Dragon Below'),('The Path of Light'),('The Undying Court'),('The Spirits of the Past'),
('Bahamut'),('Blibdoolpoolp'),('Corellon Larethian'),('Deep Sashelas'),('Eadro'),('Garl Glittergold'),('Grolantor'),
('Gruumsh'),('Hruggek'),('Kurtulmak'),('Laogzed'),('Lolth'),('Maglubiyet'),('Moradin'),('Rillifane Rallathil'),
('Sehanine Moonbow'),('Sekolah'),('Semuanya'),('Skerrit'),('Skoraeus Stonebones'),('Surtur'),('Thrym'),('Tiamat'),('Yondalla')

DECLARE @firstnamecount INT
SELECT @firstnamecount = COUNT(*) FROM @firstnametemp

-- Update Names
UPDATE
	tblNameActual
SET
	[First] = (SELECT name FROM @firstnametemp WHERE id=firstnametempid)
				FROM
			  (SELECT na.NameID,
						na.First,
						abs(checksum(newid()))%@firstnamecount +1 as firstnametempid
			   FROM tblNameActual na) fn
UPDATE
	tblNameActual
SET
	[Middle] = NULL

UPDATE
	tblNameActual
SET
	[Last] = (SELECT name FROM @nametemp WHERE id=nametempid)
				FROM
			(SELECT na.NameID,
					na.Last,
					abs(checksum(newid()))%@namecount +1 as nametempid
			FROM tblNameActual na) sq

UPDATE
	tblNameActual
SET
	[DOB] = DATEADD(DAY, (ABS(CHECKSUM(NEWID()) % 3650))*-1, DOB)

-- Update numbers
DECLARE @tblNumberIDs AS TABLE
(
	NumberID INT
)

INSERT INTO
	@tblNumberIDs
SELECT
	NumberID
FROM
	tblNameNumber

UPDATE n
SET n.Number = dbo.fnGetNewNumber(LEN(n.Number), n.NumberType)
FROM
	tblNumber n
	JOIN tblNameNumber nn ON nn.NumberID = n.NumberID

--DECLARE @cnt int, @total int

--SELECT @total = COUNT(1) FROM @tblNumberIDs
--SET @cnt = 0

--WHILE ((SELECT COUNT(*) FROM @tblNumberIDs) > 0) BEGIN
--	DECLARE @NumID AS INT = (SELECT TOP(1) NumberID FROM @tblNumberIDs ORDER BY NumberID)
--	DECLARE @NameNumID AS INT
--	DECLARE @NameID AS INT
--	DECLARE @Type AS CHAR(5)
--	DECLARE @OldNumLEN AS INT
--	DECLARE @Active AS BIT
--	DECLARE @NumberType AS CHAR(5)

--	SELECT 
--		@Type = nm.NumberType, 
--		@OldNumLEN = LEN(nm.Number),
--		@NameID = nn.NameID,
--		@NameNumID = nn.NameNumberID,
--		@Active = nn.Active,
--		@NumberType = nm.NumberType
--	FROM 
--		tblNumber AS nm
--		JOIN tblNameNumber AS nn ON nn.NumberID = nm.NumberID
--	WHERE 
--		nm.NumberID = @NumID
	
--	DECLARE @NewNum AS VARCHAR(255);

--	IF (@Type = 'SSN') BEGIN
--		DECLARE @Num1 AS VARCHAR(3) = CAST(dbo.fnGetRandomNumber(3) AS VARCHAR(3));
--		DECLARE @Num2 AS VARCHAR(2) = CAST(dbo.fnGetRandomNumber(3) AS VARCHAR(3));
--		DECLARE @Num3 AS VARCHAR(4) = CAST(dbo.fnGetRandomNumber(3) AS VARCHAR(3));
--		SET @NewNum = @Num1 + '-' + @Num2 + '-' + @Num3
--	END ELSE BEGIN
--		SET @NewNum = dbo.fnGetRandomString(@OldNumLEN);
--	END
--	PRINT 'Updating ' + CAST(@cnt as varchar(max)) + ' of ' + CAST(@total as varchar(max))
--	EXEC dbo.devUpdateNameNumber @NumID, @NameID, @NumID, @Active, @NumberType, @NewNum
--	DELETE FROM @tblNumberIDs WHERE NumberID = @NumID-- (SELECT TOP(1) NumberID FROM @tblNumberIDs ORDER BY NumberID)
--END


DECLARE @tempStreetNames Table
(
	id int IDENTITY(1,1) PRIMARY KEY,
	streetName varchar(50)
)
--Classes and subclasses from all official D&D 5e materials, including test materials
INSERT INTO @tempStreetNames (streetName)
VALUES
('Alchemist'), ('Artillerist'), ('Battle Smith'),	('Artificer'),
('Ancestral Guardian'), ('Battlerager'), ('Berserker'), ('Storm Herald'), ('Totem'), ('Wild Soul'), ('Zealot'), ('Barbarian'),
('Eloquence'), ('Glamour'), ('Lore'),	('Swords'), ('Valor'), ('Whispers'), ('Bard'),
('Ghostslayer'), ('Lycan'), ('Mutant'), ('Profane Soul'), ('Blood Hunter'),
('Arcana'), ('Death'), ('Grave'), ('Knowledge'), ('Life'), ('Light'), ('Nature'), ('Order'), ('Tempest'), ('Trickery'), ('Twilight'), ('War'), ('Cleric'),
('Dreams'), ('Spores'), ('Land'), ('Moon'),	('Shepherd'),	('Wildfire'),	('Druid'),
('Arcane Archer'), ('Battle Master'), ('Cavalier'), ('Champion'), ('Eldritch Knight'), ('Gunslinger'), ('Psychic Warrior'), ('Purple Dragon Knight'), ('Rune Knight'), ('Samurai'),	('Fighter'),
('Astral Self'), ('Drunken Master'), ('Four Elements'), ('Kensei'), ('Long Death'), ('Open Hand'), ('Sun Soul'), ('Monk'),
('Conquest'), ('Devotion'), ('Heroism'), ('Redemption'), ('Ancients'), ('Crown'), ('Vengeance'), ('Oathbreaker'), ('Paladin'),
('Beast Master'), ('Gloom Stalker'), ('Horizon Walker'), ('Hunter'), ('Monster Slayer'), ('Swarmkeeper'), ('Ranger'),
('Arcane Trickster'), ('Assassin'), ('Inquisitive'), ('Mastermind'), ('Scout'), ('Soulknife'), ('Swashbuckler'), ('Revived'), ('Thief'), ('Rogue'),
('Aberrant Mind'), ('Divine Soul'), ('Draconic Bloodline'), ('Shadow Magic'), ('Storm Sorcery'), ('Wild Magic'), ('Sorcerer'),
('Archfey'), ('Celestial'), ('Fiend'), ('Great Old One'), ('Hexblade'), ('Lurker in the Deep'), ('Undying'), ('Warlock'),
('Bladesinging'), ('Onomancy'), ('Psionics'), ('Abjuration'), ('Conjuration'), ('Divination'), ('Enchantment'), ('Evocation'), ('Illusion'), ('Necromancy'), ('Transmutation'), ('War Magic'), ('Wizard')

DECLARE @tempStreetType TABLE
(
	id int IDENTITY(1,1) PRIMARY KEY,
	street varchar(50)
)

INSERT INTO @tempStreetType (street)
VALUES
	(' road'),(' street'),(' boulevard'),(' court'),(' lane'),(' avenue'),(' way'),(' terrace')

DECLARE @streetNameCount INT
SELECT @streetNameCount = COUNT(*) FROM @tempStreetNames
DECLARE @streetTypeCount INT
SELECT @streetTypeCount = COUNT(*) From @tempStreetType

UPDATE
tblAddressActual
SET
	[Address] = CAST(sn.streetNumber AS nvarchar(max)) + ' ' +
	(SELECT streetName FROM @tempStreetNames WHERE id=streetNameTempID) +
	(SELECT street FROM @tempStreetType WHERE id = streetTypeTempID)

FROM
	(
		SELECT ad.AddressID,
			ad.Address,
			abs(checksum(newid()))%@streetNameCount +1 as streetNameTempID,
			abs(checksum(newid()))%@streetTypeCount +1 as streetTypeTempID,
			abs(checksum(newid()))%CAST(FLOOR(RAND()*(9999-100+1))+100 as INT) as streetNumber
		FROM tblAddressActual ad
	) sn

--Scramble Emails
DECLARE @tempEmailType TABLE
(
	id int IDENTITY(1,1) PRIMARY KEY,
	emailtypes varchar(50)
)
INSERT INTO @tempEmailType 
VALUES
	('@gmail.com'),
	('@yahoo.com'),
	('@hotmail.com'),
	('@RealEmail.com')

DECLARE @emailTypeCount INT
SELECT @emailTypeCount = COUNT(*) FROM @tempEmailType

UPDATE
	tblEmailActual
SET
	[Address] = en.Last + CAST(en.EmailNumber as nvarchar(max)) +
	(SELECT emailtypes FROM @tempEmailType WHERE id = emailTypeTempID)
FROM
	(SELECT na.Last,
		abs(checksum(newid()))%CAST(FLOOR(RAND()*(9999-100+1))+100 as INT) as EmailNumber,
		abs(checksum(newid()))%@emailTypeCount +1 as emailTypeTempID
	FROM tblNameActual na inner join tblEmailActual ea on na.NameID = ea.NameID) en

--Scramble phone numbers
UPDATE
	tblPhoneActual
SET
	Number = '(' +
		(RIGHT('000'+ISNULL(phn.areaCode,''),3)) +
		') ' +
		(RIGHT('000'+ISNULL(phn.firstThree,''),3)) +
		'-' +
		(RIGHT('0000'+ISNULL(phn.lastFour,''),3))
FROM
	(SELECT ph.NameID,
		abs(checksum(newid()))%CAST(FLOOR(RAND()*(999-0+1))+0 as INT) as areaCode,
		abs(checksum(newid()))%CAST(FLOOR(RAND()*(999-0+1))+0 as INT) as firstThree,
		abs(checksum(newid()))%CAST(FLOOR(RAND()*(9999-0+1))+0 as INT) as lastFour
	FROM tblPhoneActual ph) phn

--Scramble Charges
UPDATE
	tblCountActual
SET
	IncidentDt1 = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 7300), '1990-01-01'),
	IncidentDt2 = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 7300), '2000-01-01')

-- scramble notes columns
DECLARE @scrambleQuery nvarchar(max)
DECLARE @scrambleCursor CURSOR

SET @scrambleCursor = CURSOR FOR
SELECT  'UPDATE '+ic.TABLE_NAME+' SET Notes = dbo.ipsumLoremGen(10) WHERE Notes != ''''' as querystring
FROM INFORMATION_SCHEMA.COLUMNS ic
INNER JOIN INFORMATION_SCHEMA.TABLES it ON ic.TABLE_NAME = it.TABLE_NAME
	WHERE it.TABLE_SCHEMA = 'dbo'
	AND it.TABLE_TYPE = 'BASE TABLE'
	AND it.TABLE_NAME NOT LIKE 'ltbl%'
	AND it.TABLE_NAME NOT LIKE 'lctbl%'
	AND it.TABLE_NAME NOT LIKE 'ljweb%'
	AND ic.TABLE_CATALOG = 'UintaTest'
	AND COLUMN_NAME = 'notes'
	AND DATA_TYPE = 'varchar'
	AND (CHARACTER_MAXIMUM_LENGTH = -1 OR CHARACTER_MAXIMUM_LENGTH > 255)

OPEN @scrambleCursor
FETCH NEXT
FROM @scrambleCursor INTO @scrambleQuery
WHILE @@FETCH_STATUS = 0
BEGIN
    EXEC sp_executesql @stmt=@scrambleQuery
	FETCH NEXT
	FROM @scrambleCursor INTO @scrambleQuery
END

CLOSE @scrambleCursor
DEALLOCATE @scrambleCursor
--scramble notes colums that are not called 'Notes'
UPDATE
	tblSuspectFileVersion
SET
	EventNotes = dbo.ipsumLoremGen(6)
WHERE EventNotes != ''

UPDATE
	jb_admMessageQueue
SET
	Note = dbo.ipsumLoremGen(6)
WHERE Note != ''

UPDATE
	tblCountActual
SET
	Notes2 = dbo.ipsumLoremGen(6)
WHERE Notes2 != ''

UPDATE
	tblCountInvPersActual
SET
	Notes2 = dbo.ipsumLoremGen(6)
WHERE Notes2 != ''

UPDATE
	ltblCountInvPersActual
SET
	Notes2 = dbo.ipsumLoremGen(6)
WHERE Notes2 != ''

UPDATE
	ltblCountActual
SET
	Notes2 = dbo.ipsumLoremGen(6)
WHERE Notes2 != ''

--truncate notes that are image type
TRUNCATE TABLE tblCaseNotes
TRUNCATE TABLE tblNameNotes

--truncate logging tables

DECLARE @truncateQuery nvarchar(max)
DECLARE @truncateCursor CURSOR

SET @truncateCursor = CURSOR FOR
SELECT  'TRUNCATE TABLE dbo.'+it.TABLE_NAME as querystring
FROM INFORMATION_SCHEMA.TABLES it
	WHERE it.TABLE_SCHEMA = 'dbo'
	AND it.TABLE_TYPE = 'BASE TABLE'
	AND ( it.TABLE_NAME LIKE 'ltbl%'
		OR it.TABLE_NAME LIKE 'lctbl%'
		OR it.TABLE_NAME LIKE 'ljweb%'
		OR it.TABLE_NAME LIKE 'ldev%'
	)

OPEN @truncateCursor
FETCH NEXT
FROM @truncateCursor INTO @truncateQuery
WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC sp_executesql @stmt=@truncateQuery
	FETCH NEXT
	FROM @truncateCursor INTO @truncateQuery
END

CLOSE @truncateCursor
DEALLOCATE @truncateCursor

-- Scramble Cases
DECLARE @tempCaseTitle TABLE
(
	id int IDENTITY(1,1) PRIMARY KEY,
	caseTitle varchar(MAX)
)

--List of mystery novels from The Top 100 Mystery Novels of All Time by the Mystery Writers Association
--https://simple.wikipedia.org/wiki/The_Top_100_Crime_Novels_of_All_Time
INSERT INTO @tempCaseTitle (caseTitle) VALUES
('The Complete Sherlock Holmes (stories and 4 novels)	~	Arthur Conan Doyle'), ('The Maltese Falcon	~	Dashiell Hammett'),
('Tales of Mystery & Imagination (stories)	~	Edgar Allan Poe'), ('The Daughter of Time	~	Josephine Tey'),
('Presumed Innocent	~	Scott Turow'), ('The Spy Who Came In From the Cold	~	John le Carre'),
('The Moonstone	~	Wilkie Collins'),('The Big Sleep	~	Raymond Chandler'),
('Rebecca	~	Daphne du Maurier'),('And Then There Were None	~	Agatha Christie'),
('Anatomy of a Murder	~	Robert Traver'),('The Murder of Roger Ackroyd	~	Agatha Christie'),
('The Long Goodbye	~	Raymond Chandler'),('The Postman Always Rings Twice	~	James M. Cain'),
('The Godfather (film)	~	Mario Puzo'),('The Silence of the Lambs (film)	~	Thomas Harris'),
('The Mask of Dimitrios 	~	Eric Ambler'),('Gaudy Night	~	Dorothy L. Sayers'),
('Witness for the Prosecution (play)	~	Agatha Christie'),('The Day of the Jackal	~	Frederick Forsyth'),
('Farewell My Lovely	~	Raymond Chandler'),('The Thirty-Nine Steps	~	John Buchan'),
('The Name of the Rose	~	Umberto Eco'),('Crime and Punishment	~	Fyodor Dostoyevsky'),
('Storm Island	~	Ken Follett'),('Rumpole of the Bailey	~	John Mortimer'),
('Red Dragon	~	Thomas Harris'),('The Nine Tailors	~	Dorothy L. Sayers'),
('Fletch	~	Gregory Mcdonald'),('Tinker, Tailor, Soldier, Spy	~	John le Carre'),
('The Thin Man	~	Dashiell Hammett'),('The Woman in White	~	Wilkie Collins'),
('Trent''s Last Case	~	E. C. Bentley'),('Double Indemnity	~	James M. Cain'),
('Gorky Park	~	Martin Cruz Smith'),('Strong Poison	~	Dorothy L. Sayers'),
('Dance Hall of the Dead	~	Tony Hillerman'),('The Hot Rock	~	Donald E. Westlake'),
('Red Harvest	~	Dashiell Hammett'),('The Circular Staircase	~	Mary Roberts Rinehart'),
('Murder on the Orient Express	~	Agatha Christie'),('The Firm	~	John Grisham'),
('The IPCRESS File	~	Len Deighton'),('Laura	~	Vera Caspary'),('I, the Jury	~	Mickey Spillane'),
('The Laughing Policeman	~	Maj Sjowall & Per Wahloo'),('Bank Shot	~	Donald E. Westlake'),
('The Third Man	~	Graham Greene'),('The Killer Inside Me (film)	~	Jim Thompson'),
('Where Are the Children?	~	Mary Higgins Clark'),('A is for Alibi	~	Sue Grafton'),
('The First Deadly Sin	~	Lawrence Sanders'),('A Thief of Time	~	Tony Hillerman'),
('In Cold Blood	~	Truman Capote'),('Rogue Male	~	Geoffrey Household'),
('Murder Must Advertise	~	Dorothy L. Sayers'),('The Innocence of Father Brown (stories)	~	G. K. Chesterton'),
('Smiley''s People	~	John le Carre'),('The Lady in the Lake	~	Raymond Chandler'),
('To Kill a Mockingbird	~	Harper Lee'),('Our Man in Havana	~	Graham Greene'),
('The Mystery of Edwin Drood	~	Charles Dickens'),('Wobble to Death	~	Peter Lovesey'),
('Ashenden (stories)	~	W. Somerset Maugham'),('The Seven Per-Cent Solution	~	Nicholas Meyer'),
('The Doorbell Rang	~	Rex Stout'),('Stick	~	Elmore Leonard'),('The Little Drummer Girl	~	John le Carre'),
('Brighton Rock	~	Graham Greene'),('Dracula	~	Bram Stoker'),('The Talented Mr. Ripley	~	Patricia Highsmith'),
('The Moving Toyshop	~	Edmund Crispin'),('A Time to Kill	~	John Grisham'),('Last Seen Wearing ...	~	Hillary Waugh'),
('Little Caesar	~	W. R. Burnett'),('The Friends of Eddie Coyle	~	George V. Higgins'),
('Clouds of Witness	~	Dorothy L. Sayers'),('From Russia, with Love	~	Ian Fleming'),
('Beast in View	~	Margaret Millar'),('Smallbone Deceased	~	Michael Gilbert'),
('The Franchise Affair	~	Josephine Tey'),('Crocodile on the Sandbank	~	Elizabeth Peters'),
('Shroud for a Nightingale	~	P. D. James'),('The Hunt for Red October	~	Tom Clancy'),
('Chinaman''s Chance	~	Ross Thomas'),('The Secret Agent	~	Joseph Conrad'),
('The Dreadful Lemon Sky	~	John D. MacDonald'),('The Glass Key	~	Dashiell Hammett'),
('A Judgement in Stone	~	Ruth Rendell'),('Come and Kill Me	~	Josephine Tey'),('The Chill	~	Ross Macdonald'),
('Devil in a Blue Dress	~	Walter Mosley'),('The Choirboys	~	Joseph Wambaugh'),('God Save the Mark	~	Donald E. Westlake'),
('Home Sweet Homicide	~	Craig Rice'),('The Hollow Man (UK)	~	John Dickson Carr'),('Prizzi''s Honour (UK)	~	Richard Condon'),
('The Steam Pig	~	James McClure'),('Time and Again	~	Jack Finney'),('A Morbid Taste for Bones	~	Ellis Peters'),
('Rosemary''s Baby	~	Ira Levin')

DECLARE @tempCaseCount INT
SELECT @tempCaseCount = COUNT(*) FROM @tempCaseTitle

UPDATE
	tblCaseActual
SET
	CaseTitle = '(NameID: '+ CAST(ct.NameID AS nvarchar(max))+') '+
	(SELECT caseTitle FROM @tempCaseTitle WHERE id = streetTypeTempID)
FROM
	(SELECT 
	CaseTitle,
	NameID,
	abs(checksum(newid()))%@firstnamecount +1 as streetTypeTempID
	FROM tblCaseActual) ct

UPDATE
	tblCaseActual
SET
	RcvdDt = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 7300), '1990-01-01'),
	StatusDt = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 7300), '2000-01-01')

DELETE FROM ltblNameActual
DELETE FROM ltblNameNumber
DELETE FROM ltblAddressActual
DELETE FROM ltblEmailActual
DELETE FROM ltblPhoneActual
DELETE FROM ltblCaseActual

-- Clean up
IF (OBJECT_ID('dbo.devRandomNumber') IS NOT NULL) DROP VIEW dbo.devRandomNumber
GO

IF (OBJECT_ID('dbo.devNewID') IS NOT NULL) DROP VIEW dbo.devNewID
GO

IF (OBJECT_ID('dbo.fnGetRandomNumber') IS NOT NULL) DROP FUNCTION dbo.fnGetRandomNumber
GO

IF (OBJECT_ID('dbo.fnGetRandomString') IS NOT NULL) DROP FUNCTION dbo.fnGetRandomString
GO

IF (OBJECT_ID('dbo.fnGetNewNumber') IS NOT NULL) DROP FUNCTION dbo.fnGetNewNumber
GO

-- sql stolen from JWMC
exec sp_fulltext_table 'tblCaseSummary', 'deactivate'

EXEC devRebuildCaseTitles
TRUNCATE TABLE dbo.tblCaseSummary

--DropTemporaryCaseSummaryIndexes.sql
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblCaseAgency]') AND [name] = N'_dta_index_tblCaseAgency_7_738869749__K7_K5_K4_K1_K2')
DROP INDEX [dbo].[tblCaseAgency].[_dta_index_tblCaseAgency_7_738869749__K7_K5_K4_K1_K2]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblCaseInvPers]') AND [name] = N'_dta_index_tblCaseInvPers_7_274152072__K3_K2_K1')
DROP INDEX [dbo].[tblCaseInvPers].[_dta_index_tblCaseInvPers_7_274152072__K3_K2_K1]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblCaseInvPers]') AND [name] = N'_dta_index_tblCaseInvPers_7_274152072__K8_K4_K3_K2_K1_K6')
DROP INDEX [dbo].[tblCaseInvPers].[_dta_index_tblCaseInvPers_7_274152072__K8_K4_K3_K2_K1_K6] 

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblCountActual]') AND [name] = N'_dta_index_tblCountActual_7_428580615__K2_K5_K1_K20_8')
DROP  INDEX [dbo].[tblCountActual].[_dta_index_tblCountActual_7_428580615__K2_K5_K1_K20_8]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblCountInvPersActual]') AND [name] = N'_dta_index_tblCountInvPersActual_7_2010490241__K8_K13_K2')
DROP  INDEX [dbo].[tblCountInvPersActual].[_dta_index_tblCountInvPersActual_7_2010490241__K8_K13_K2]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblNameActual]') AND [name] = N'_dta_index_tblName_7_833438043__K1_44')
DROP  INDEX [dbo].[tblNameActual].[_dta_index_tblName_7_833438043__K1_44] 

--TemporaryCaseSummaryIndexes.sql
CREATE NONCLUSTERED INDEX [_dta_index_tblCaseAgency_7_738869749__K7_K5_K4_K1_K2] ON [dbo].[tblCaseAgency] 
(
	[CaseID] ASC,
	[Active] ASC,
	[Lead] ASC,
	[CaseAgencyID] ASC,
	[Agency] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]

------------

CREATE NONCLUSTERED INDEX [_dta_index_tblCaseInvPers_7_274152072__K3_K2_K1] ON [dbo].[tblCaseInvPers] 
(
	[CaseID] ASC,
	[NameID] ASC,
	[CaseInvPersID] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]

----------------

CREATE NONCLUSTERED INDEX [_dta_index_tblCaseInvPers_7_274152072__K8_K4_K3_K2_K1_K6] ON [dbo].[tblCaseInvPers] 
(
	[active] ASC,
	[CaseAgencyID] ASC,
	[CaseID] ASC,
	[NameID] ASC,
	[CaseInvPersID] ASC,
	[Involvement] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]

------------------
CREATE NONCLUSTERED INDEX [_dta_index_tblCountActual_7_428580615__K2_K5_K1_K20_8] ON [dbo].[tblCountActual] 
(
	[CaseID] ASC,
	[CountNum] ASC,
	[CountID] ASC,
	[Statute] ASC
)

-----------------

--Cannot find the object "dbo.tblCountInvPersActual" because it does not exist or you do not have permissions.
--CREATE NONCLUSTERED INDEX [_dta_index_tblCountInvPersActual_7_2010490241__K8_K13_K2] ON [dbo].[tblCountInvPersActual] 
--(
--	[CaseInvPersID] ASC,
--	[Dispo] ASC,
--	[CountID] ASC
--)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]

------------------

CREATE NONCLUSTERED INDEX [_dta_index_tblName_7_833438043__K1_44] ON [dbo].[tblNameActual] 
(
	[NameID] ASC
)
INCLUDE ( [FullName2]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]

--BuildCaseSummaries.sql
DECLARE @TempIDs TABLE
(
	ID int IDENTITY(1,1) PRIMARY KEY,
	CaseID varchar(12)
)

INSERT INTO @TempIDs (CaseID)
SELECT CaseID FROM tblCaseActual ORDER BY CaseID

WHILE EXISTS (SELECT TOP 1 * FROM @TempIDs)
BEGIN

DELETE FROM dbo.tblCaseSummary WHERE CaseID IN (SELECT TOP 1000 t.CaseID FROM @TempIDs t ORDER BY t.ID)

INSERT INTO dbo.tblCaseSummary
(CaseID, Summary, HighRankSummary, ResultsSummary, 
NameID, CaseTitle, LawNumTypeDesc, LawNumAgencyDesc, LawNum, 
LawNumLeadFullName2, LawNumInvPersonTypeDesc, CourtNumTypeDesc, CourtNumAgencyDesc, CourtNum, 
CourtNumLeadFullName2, CourtNumInvPersonTypeDesc, ProsNumTypeDesc, ProsNumAgencyDesc, ProsNum, 
ProsNumLeadFullName2, ProsNumInvPersonTypeDesc, DefNumTypeDesc, DefNumAgencyDesc, Defnum, 
DefNumLeadFullName2, DefNumInvPersonTypeDesc, MiscNumTypeDesc, MiscNumAgencyDesc, Miscnum, 
MiscNumLeadFullName2, MiscNumInvPersonTypeDesc, IncidentDt1, RcvdDt, StatuteCombined, Disposition, 
CaseStatusCode, CaseStatusDesc, CaseTypeCode, CaseTypeDesc, InvPers, PhysicalLocation,
ModDt, CaseStatusMasterCode
)
SELECT TOP 1000
	t.CaseID, 
	dbo.CreateCaseSummary(t.CaseID),
	dbo.CreateHighRankCaseSummary(t.CaseID),
	ResultsSummary = sr.Summary,
	sr.NameID,
	sr.CaseTitle,
	sr.LawNumTypeDesc,
	sr.LawNumAgencyDesc,
	sr.LawNum,
	sr.LawNumLeadFullName2,
	sr.LawNumInvPersonTypeDesc,

	sr.CourtNumTypeDesc,
	sr.CourtNumAgencyDesc,
	sr.CourtNum,
	sr.CourtNumLeadFullName2,
	sr.CourtNumInvPersonTypeDesc,

	sr.ProsNumTypeDesc,
	sr.ProsNumAgencyDesc,
	sr.ProsNum,
	sr.ProsNumLeadFullName2,
	sr.ProsNumInvPersonTypeDesc,

	sr.DefNumTypeDesc,
	sr.DefNumAgencyDesc,
	sr.DefNum,
	sr.DefNumLeadFullName2,
	sr.DefNumInvPersonTypeDesc,

	sr.MiscNumTypeDesc,
	sr.MiscNumAgencyDesc,
	sr.MiscNum,
	sr.MiscNumLeadFullName2,
	sr.MiscNumInvPersonTypeDesc,

	sr.IncidentDt1,
	sr.RcvdDt,
	sr.StatuteCombined,
	sr.Disposition,
	sr.CaseStatusCode,

	sr.CaseStatusDesc,
	sr.CaseTypeCode,
	sr.CaseTypeDesc,
	sr.InvPers,
	sr.PhysicalLocation,
	sr.ModDt,
	sr.CaseStatusMasterCode
FROM @TempIDs t
	INNER JOIN dbo.devCaseSummarySearchResults sr ON t.CaseID = sr.CaseID
ORDER BY t.ID

DELETE FROM @TempIDs
WHERE ID IN (SELECT TOP 1000 ID FROM @TempIDs ORDER BY ID)

END

--DropTemporaryCaseSummaryIndexes.sql
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblCaseAgency]') AND [name] = N'_dta_index_tblCaseAgency_7_738869749__K7_K5_K4_K1_K2')
DROP INDEX [dbo].[tblCaseAgency].[_dta_index_tblCaseAgency_7_738869749__K7_K5_K4_K1_K2]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblCaseInvPers]') AND [name] = N'_dta_index_tblCaseInvPers_7_274152072__K3_K2_K1')
DROP INDEX [dbo].[tblCaseInvPers].[_dta_index_tblCaseInvPers_7_274152072__K3_K2_K1]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblCaseInvPers]') AND [name] = N'_dta_index_tblCaseInvPers_7_274152072__K8_K4_K3_K2_K1_K6')
DROP INDEX [dbo].[tblCaseInvPers].[_dta_index_tblCaseInvPers_7_274152072__K8_K4_K3_K2_K1_K6] 

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblCountActual]') AND [name] = N'_dta_index_tblCountActual_7_428580615__K2_K5_K1_K20_8')
DROP  INDEX [dbo].[tblCountActual].[_dta_index_tblCountActual_7_428580615__K2_K5_K1_K20_8]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblCountInvPersActual]') AND [name] = N'_dta_index_tblCountInvPersActual_7_2010490241__K8_K13_K2')
DROP  INDEX [dbo].[tblCountInvPersActual].[_dta_index_tblCountInvPersActual_7_2010490241__K8_K13_K2]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblNameActual]') AND [name] = N'_dta_index_tblName_7_833438043__K1_44')
DROP  INDEX [dbo].[tblNameActual].[_dta_index_tblName_7_833438043__K1_44] 
--end DropTemporaryCaseSummaryIndexes.sql

exec sp_fulltext_table 'tblCaseSummary', 'activate'


--TODO: scramble charges, currently date and notes are scrambled, but perhaps other aspects should also be scrambled
--TODO: scramble birthdays
--TODO: give values to  middle names
