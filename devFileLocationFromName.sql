CREATE VIEW [dbo].[devFileLocationFromName]
AS
	SELECT
		e.CaseID,
		ip.NameID,
		e.Title as [FileName],
		v.VersionNumber,
		f.Location
	FROM
		tblEventActual e
	INNER JOIN
		dbo.ctblEventSuperType est ON e.EventType = est.Code
	LEFT JOIN
		dbo.tblFileVersionGroup g ON g.EventID = e.EventID
	LEFT JOIN
		dbo.tblFileVersion v ON v.FileVersionGroupID = g.FileVersionGroupID
	LEFT JOIN
		dbo.tblFile f ON v.FileID = f.FileID
	LEFT JOIN
		dbo.tblEventInvPers ip ON e.EventID = ip.EventID
	WHERE 
		est.CodeType IN (2, 4)
GO